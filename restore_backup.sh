#!/bin/bash

RESTORE_WORKAREA="/tmp/tmp_restore"
MYSQL_USER="root"
MYSQL_PW="MalaysiaPilot109"
RESTORE_FILENAME=`basename $1`

#check if apache is running, if yes, shut it down
ps auxw | grep httpd | grep -v grep > /dev/null

if [ $? == 0 ]
then 
  {
    /etc/init.d/httpd stop > /dev/null 
  }
fi

#hey! did you give me a valid backup to restore, lemme check!
if [ ! -f $1 ] 
then
  {
    echo "Backup file not found .."
    exit 1
  }
fi

#lets make a separate place to mess about in
if [ ! -d $RESTORE_WORKAREA ]
then 
  {
    mkdir -p $RESTORE_WORKAREA
  }
fi

cp $1 $RESTORE_WORKAREA/
tar -xf $RESTORE_WORKAREA/$RESTORE_FILENAME -C $RESTORE_WORKAREA/ 
rm -f $RESTORE_WORKAREA/$RESTORE_FILENAME
rm -f $RESTORE_WORKAREA/urlenvironment
rm -f $RESTORE_WORKAREA/VERSION.txt
MYSQL_DB_NAME=`mysql -B -N -u$MYSQL_USER -p$MYSQL_PW -e "show databases" | grep frogos`
mysql -B -N -u$MYSQL_USER -p$MYSQL_PW -e "drop database $MYSQL_DB_NAME"
mysql -B -N -u$MYSQL_USER -p$MYSQL_PW -e "create database $MYSQL_DB_NAME"
mysql -B -N -u$MYSQL_USER -p$MYSQL_PW $MYSQL_DB_NAME < $RESTORE_WORKAREA/db.sql
SCHOOL_CODE=`echo $MYSQL_DB_NAME | sed s/_/\\n/g | grep -v "frog"`
rm -rf /srv/sites/$SCHOOL_CODE/user
tar -xf $RESTORE_WORKAREA/user.tgz -C /srv/sites/$SCHOOL_CODE/ 
rm -rf $RESTORE_WORKAREA
chown -R apache: /srv/sites/$SCHOOL_CODE
/etc/init.d/httpd start > /dev/null
